# Copied from: https://github.com/janet-lang/spork/blob/641b27238e073c5f5f963ec16c79b51643d3e66f/spork/msg.janet

(defn make-recv
  "Get a function that, when invoked, gets the next message from a readable stream.
  Provide an optional unpack function that will parse the received buffer."
  [stream &opt unpack]
  (def buf @"")
  (default unpack string)
  (fn receiver []
    (buffer/clear buf)
    (if-not (:chunk stream 4 buf) (break))
    (def [b0 b1 b2 b3] buf)
    (def len (+ b0 (* b1 0x100) (* b2 0x10000) (* b3 0x1000000)))
    (buffer/clear buf)
    (if-not (:chunk stream len buf) (break))
    (unpack (string buf))))

(defn make-send
  "Create a function that when called with a msgs sends that msg.
  Provide an optional pack function that will convert a message to a string."
  [stream &opt pack]
  (def buf @"")
  (default pack string)
  (fn sender [msg]
    (def x (pack msg))
    (buffer/clear buf)
    (buffer/push-word buf (length x))
    (buffer/push-string buf x)
    (:write stream buf)
    (:flush stream)
    nil))
