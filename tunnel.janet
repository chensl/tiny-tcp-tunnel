(use ./ev-utils)
(use ./msg)
(use ./net-utils)

(var nonce 0)
(def nonce-lock (ev/lock))

(defn gen-nonce
  []
  (ev/acquire-lock nonce-lock)
  (defer (ev/release-lock nonce-lock)
    (++ nonce)
    nonce))

(defn server
  [host port]
  (eprint "server listening: addr=" (join-host-port host port))

  (def conns @{})
  (def conns-lock (ev/lock))

  (defn put-conn [id conn]
    (ev/acquire-lock conns-lock)
    (defer (ev/release-lock conns-lock))
    (put conns id conn))

  (defn get-and-delete-conn [id]
    (ev/acquire-lock conns-lock)
    (defer (ev/release-lock conns-lock))
    (def conn (get conns id))
    (if conn (put conns id nil))
    conn)

  (net/server
    host port
    (fn [stream]
      (defer (:close stream)
        (def recv (make-recv stream unmarshal))
        (def send (make-send stream marshal))

        (match (recv)
          [:hello]
          (do
            (def ln (net/listen "0.0.0.0" "0"))
            (defer (:close ln)
              (def [_ port] (net/localname ln))
              (send [:hello port])

              (def client-addr (join-host-port ;(net/peername stream)))
              (eprint "client connected: addr=" client-addr)

              (def n (nursery))
              (spawn-nursery
                n
                (forever
                  (ev/sleep 10)
                  (send [:heartbeat])))
              (spawn-nursery
                n
                (forever
                  (def conn (net/accept ln))
                  (def id (gen-nonce))
                  (put-conn id conn)
                  (ev/spawn
                    (ev/sleep 10)
                    (def conn (get-and-delete-conn id))
                    (when conn
                      (:close conn)
                      (eprint "removed stale connection: id=" id)))
                  (send [:connection id])))

              (try (join-nursery n)
                ([err]
                  (eprint "client disconnected: addr=" client-addr " err=" err)))))

          [:accept id]
          (do
            # (eprint "forwarding connection: id=" id)
            (def conn (get-and-delete-conn id))
            (if-not conn
              (eprint "missing connection: id=" id)
              (proxy stream conn)))

          _ (eprint "invalid message"))))))

(defn client
  [host port local-host local-port]
  (def stream (net/connect host port))
  (def recv (make-recv stream unmarshal))
  (def send (make-send stream marshal))

  (send [:hello])
  (match (recv)
    [:hello port] (eprint "tunnel server listening at: tcp://" (join-host-port host port))
    _ (do (eprint "unexpected initial non-hello message") (break)))

  (forever
    (match (recv)
      [:heartbeat] nil
      [:connection id]
      (ev/spawn
        (def remote-stream (net/connect host port))
        (def local-stream (net/connect local-host local-port))
        ((make-send remote-stream marshal) [:accept id])
        (proxy remote-stream local-stream)))))
