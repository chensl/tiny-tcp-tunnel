{
  description = "tiny-tcp-tunnel";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = {
    self,
    nixpkgs,
    flake-utils,
    ...
  }:
    flake-utils.lib.eachDefaultSystem (
      system: let
        pkgs = import nixpkgs {inherit system;};
      in {
        devShells.default = pkgs.mkShell {
          nativeBuildInputs = with pkgs; [
            janet
            jpm
          ];
          shellHook = ''
            export JANET_TREE="$PWD/.jpm_tree"
            export JANET_PATH="$JANET_TREE/lib"
            export PATH="$JANET_TREE/bin:$PATH"
          '';
        };
      }
    );
}
