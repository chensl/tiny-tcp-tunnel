(use ./ev-utils)

(defn copy
  [dst src]
  (defer (:close dst)
    (def buf @"")
    (forever
      (buffer/clear buf)
      (def bytes (net/read src 32768 buf))
      (if-not bytes
        (break)
        (net/write dst bytes)))))

(defn proxy
  [s1 s2]
  (def n (nursery))
  (spawn-nursery n (copy s1 s2))
  (spawn-nursery n (copy s2 s1))
  (join-nursery n))

(defn join-host-port
  [host port]
  (if (string/find host ":")
    (string "[" host "]" ":" port)
    (string host ":" port)))
