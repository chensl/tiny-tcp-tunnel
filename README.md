# tiny-tcp-tunnel

```bash
$ ./bin/tunnel-client --help
Usage: ./bin/tunnel-client <port | host port>

$ ./bin/echo-server
server listening: addr=localhost:8080

$ ./bin/tunnel-client 8080
tunnel server listening at: tcp://tun.iferr.dev:16327

$ telnet tun.iferr.dev 16327
Trying tun.iferr.dev...
Connected to tun.iferr.dev.
Escape character is '^]'.
foo
foo
bar
bar
^]
telnet> ^D
Connection closed.
```
